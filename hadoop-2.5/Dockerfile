FROM openjdk-7:1.0
MAINTAINER cuongluu <luutuancuong@gmail.com>

####*******************************************************************Hadoop2.5*******************************************************

#********Step: Create a hadoop user
USER root

RUN groupadd hd-group
RUN useradd -g hd-group hd-user

#********Step: Download hadoop
WORKDIR /opt

#way1: use ADD
ADD res/hadoop-2.5.2.tar.gz /opt/hadoop-2.5.2.tar.gz
RUN mv hadoop-2.5.2.tar.gz/hadoop-2.5.2 ./hadoop
RUN rm -rf hadoop-2.5.2.tar.gz

#way2: use wget
#RUN wget http://mirrors.viethosting.vn/apache/hadoop/common/hadoop-2.5.2/hadoop-2.5.2.tar.gz
#RUN tar xvzf hadoop-2.5.2.tar.gz
#RUN mv hadoop-2.5.2 ./hadoop

RUN chown hd-user:hd-group /opt/hadoop -R

#********Step: Config
USER hd-user

ADD res/core-site.xml /opt/hadoop/etc/hadoop/core-site.xml_template
ADD res/hdfs-site.xml /opt/hadoop/etc/hadoop/hdfs-site.xml
ADD res/mapred-site.xml /opt/hadoop/etc/hadoop/mapred-site.xml
ADD res/yarn-site.xml /opt/hadoop/etc/hadoop/yarn-site.xml

RUN echo "#!/bin/bash" > /opt/hadoop/etc/hadoop/update_hostname
RUN echo "sed s/localhost/\$HOSTNAME/ /opt/hadoop/etc/hadoop/core-site.xml_template > /opt/hadoop/etc/hadoop/core-site.xml" >> /opt/hadoop/etc/hadoop/update_hostname
RUN chmod +x /opt/hadoop/etc/hadoop/update_hostname

USER root
RUN echo "\n" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "[program:update_hostname]" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "user=hd-user" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "command=/opt/hadoop/etc/hadoop/update_hostname" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "startsecs=0" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "autorestart=false" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "priority=999" >> /etc/supervisor/conf.d/supervisord.conf

#********Step: Passwordless ssh
USER root
RUN mkdir /home/hd-user
RUN mkdir /home/hd-user/.ssh

RUN rm -f /etc/ssh/ssh_host_dsa_key /etc/ssh/ssh_host_rsa_key /home/hd-user/.ssh/id_rsa
RUN ssh-keygen -q -N "" -t dsa -f /etc/ssh/ssh_host_dsa_key 
RUN ssh-keygen -q -N "" -t rsa -f /etc/ssh/ssh_host_rsa_key
RUN ssh-keygen -q -N "" -t rsa -f /home/hd-user/.ssh/id_rsa
RUN cp /home/hd-user/.ssh/id_rsa.pub /home/hd-user/.ssh/authorized_keys

RUN chown hd-user:hd-group /home/hd-user -R

#********Step: HDFS
USER root
RUN mkdir -p /var/data/hadoop/hdfs/nn
RUN mkdir -p /var/data/hadoop/hdfs/snn
RUN mkdir -p /var/data/hadoop/hdfs/dn

RUN chown hd-user:hd-group /var/data/hadoop/hdfs -R

USER hd-user
WORKDIR /opt/hadoop
RUN ./bin/hdfs namenode -format


#********Step: Start Hadoop
USER root

RUN echo "\n" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "[program:hdfs_nn]" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "user=hd-user" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "command=/opt/hadoop/sbin/hadoop-daemon.sh start namenode" >> /etc/supervisor/conf.d/supervisord.conf

RUN echo "\n" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "[program:hdfs_snn]" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "user=hd-user" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "command=/opt/hadoop/sbin/hadoop-daemon.sh start secondarynamenode" >> /etc/supervisor/conf.d/supervisord.conf

RUN echo "\n" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "[program:hdfs_dn]" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "user=hd-user" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "command=/opt/hadoop/sbin/hadoop-daemon.sh start datanode" >> /etc/supervisor/conf.d/supervisord.conf

RUN echo "\n" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "[program:yarn_rm]" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "user=hd-user" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "command=/opt/hadoop/sbin/yarn-daemon.sh start resourcemanager" >> /etc/supervisor/conf.d/supervisord.conf

RUN echo "\n" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "[program:yarn_nm]" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "user=hd-user" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "command=/opt/hadoop/sbin/yarn-daemon.sh start nodemanager" >> /etc/supervisor/conf.d/supervisord.conf

RUN echo "export PATH=\$PATH:/opt/hadoop/bin/" >> /etc/profile

EXPOSE 50070 8088 9000


####*******************************************************************CMD*******************************************************
RUN env | grep _ >> /etc/environment

CMD ["/usr/bin/supervisord"]

